# Boite à musique

[https://projects.raspberrypi.org/en/projects/gpio-music-box](https://projects.raspberrypi.org/en/projects/gpio-music-box)

GPIO music box
Make a device that plays music when you press its
buttons

## Step 1 Introduction

Dans ce projet, vous allez construire une "boîte à musique" contrôlée par des boutons qui joue différents sons lorsque différents boutons sont pressés. Vous pouvez trouver une version abrégée de ce projet sur YouTube (<https://www.youtube.com/watch?v=2izvSzQ>
WYak&feature=youtu.be).
-Ce que vous allez fabriquer

- Ce que vous allez apprendre
  - Jouer des sons en Python avec pygame
  - Utiliser la bibliothèque gpiozero de Python pour connecter les pressions sur les boutons à des appels de fonction.
  - Utiliser la structure de données dictionnaire en Python.
- Ce dont vous aurez besoin
  - Matériel
    - Un ordinateur Raspberry Pi
    - Une planche à pain
        Quatre (4) interrupteurs tactiles (pour faire des boutons)
    - Cinq (5) câbles de liaison mâle-femelle
    - Quatre (4) fils de connexion mâle-mâle
    - Des haut-parleurs ou des écouteurs

## Étape 2 Configurer votre projet

Vous aurez besoin de quelques échantillons sonores pour ce projet. Il existe de nombreux fichiers sonores sur Raspbian, mais il peut être un peu difficile de les lire avec Python. Cependant, vous pouvez convertir les fichiers sonores dans un autre format de fichier que vous pourrez utiliser plus facilement dans Python.

### Création de répertoires sur un Raspberry Pi

Il existe deux façons de créer des répertoires sur le Raspberry Pi. La première utilise l'interface graphique, et la seconde utilise le terminal.

#### Utilisation de l'interface graphique

- Ouvrez une fenêtre du gestionnaire de fichiers en cliquant sur l'icône dans le coin supérieur gauche de l'écran.
- Dans la fenêtre, cliquez avec le bouton droit de la souris et sélectionnez Créer un nouveau... puis Dossier dans le menu contextuel.
- Dans la boîte de dialogue, tapez le nom de votre nouveau répertoire, puis cliquez sur OK.

#### Utilisation du terminal

- Ouvrez une nouvelle fenêtre de Terminal en cliquant sur l'icône dans le coin supérieur gauche de l'écran.
Tout d'abord, dans votre répertoire personnel, créez un répertoire appelé gpio-music-box. Vous utiliserez ce nouveau répertoire pour
stocker tous vos fichiers pour le projet.
Créez un nouveau répertoire en utilisant la commande mkdir

```console
mkdir mon-nouveau-répertoire
```

Vous pouvez lister le contenu du répertoire actuel à l'aide de la commande `ls`.
Utilisez la commande `cd` pour accéder à votre nouveau répertoire

```console
cd mon-nouveau-répertoire
```

### Copie des échantillons de sons

Il existe de nombreux échantillons de sons stockés dans le répertoire `/usr/share/sonic-pi/samples`. Dans l'étape suivante, vous allez copier ces sons dans le répertoire `gpio-music-box/samples`. Lorsque vous aurez fait cela, vous devriez être en mesure de voir tous les fichiers sonores `.flac` dans le répertoire samples.

- Utilisez la même méthode que précédemment pour créer un nouveau répertoire appelé `samples` dans votre répertoire `gpio-music-box`.
- Cliquez sur l'icône dans le coin supérieur gauche de votre écran pour ouvrir une fenêtre de terminal.
- Tapez les lignes suivantes pour copier tous les fichiers d'un répertoire à l'autre :

```console
cp /usr/share/sonic-pi/samples/*~/gpio-music-box/samples/.
```

### Convertir les fichiers sonores

Pour lire les fichiers sonores avec Python, vous devez convertir les fichiers `.flac` en fichiers `.wav`.
Si vous souhaitez en savoir plus sur la conversion des fichiers multimédias et l'exécution de commandes sur plusieurs fichiers, consultez les deux sections suivantes
sections ci-dessous.
Conversion des fichiers multimédias
Vous pouvez facilement convertir des fichiers multimédia sur le Raspberry Pi en utilisant un logiciel appelé mpeg. Ce logiciel est préinstallé
sur les dernières versions de Raspbian (<https://www.raspberrypi.org/downloads/raspbian/>).
Pour convertir des fichiers audio ou vidéo d'un format à un autre, vous pouvez utiliser cette commande de base :
ffmpeg -i input.ext1 output.ext2
Par exemple, pour convertir un fichier wav (.wav) en un fichier mp3 (.mp3), vous devez taper :
ffmpeg -i ma_musique.wav ma_musique.mp3
Exécution d'opérations par lots sur des fichiers
Exécution d'opérations par lots sur des fichiers à l'aide de bash
Qu'est-ce qu'une opération par lot ?
Renommer un fichier à l'aide de bash est assez facile. Vous pouvez utiliser la commande mv par exemple.
mv nom_fichier_original.txt nom_fichier_nouveau.txt
Dans un terminal, allez dans votre répertoire d'échantillons.
cd ~/gpio-music-box/samples
Mais que faire si vous avez besoin de renommer un millier de fichiers ?
Une opération par lot consiste à utiliser une commande ou un ensemble de commandes sur plusieurs fichiers.
Essayez votre première opération de base par lot, en utilisant n'importe quel répertoire qui contient quelques fichiers. La première chose à essayer est
quelque chose de trivial. Par exemple, vous pouvez recréer la commande ls de sorte que ;
pour chaque fichier du répertoire,
le nom du fichier soit affiché dans le terminal.
La première partie consiste à dire à bash que vous voulez opérer sur tous les fichiers du répertoire.
for f in*
f représentera chaque nom de fichier du répertoire l'un après l'autre.
Ensuite, vous devez indiquer à bash ce qu'il doit faire avec chaque nom de fichier. Dans ce cas, vous voulez faire écho du nom du fichier sur la
sortie standard.
do
echo $f
Ici, le signe `$` est utilisé pour indiquer que l'on parle de la variable f. Enfin, vous devez indiquer à bash que vous avez terminé.
done
Vous pouvez appuyer sur la touche Entrée après chaque commande si vous le souhaitez, et bash n'exécutera pas la boucle complète tant que vous n'aurez pas tapé done, de sorte que la commande
ressemblerait à ceci dans votre fenêtre de terminal :
for f in*.txt
> do
> echo $f
> done

Au lieu d'appuyer sur la touche Entrée après chaque ligne, vous pouvez également utiliser un ; pour séparer les commandes.
for f in*.txt ; do echo $f ; done

Manipulation de chaînes de caractères.

Cette dernière commande était un peu inutile, mais vous pouvez faire beaucoup plus avec les opérations par lots. Par exemple, et si vous vouliez changer le nom de chacun des fichiers, pour qu'au lieu de s'appeler 0.txt par exemple, ils s'appellent
0.md.
Essayez cette commande :

```bash
for f in *.txt; do mv "$f" "${f%.txt}.md"; done
```

Si vous ls le contenu du répertoire, vous verrez que tous les fichiers ont été renommés. Comment cela a-t-il fonctionné ?
La première partie de do f in*.txt indique à bash d'exécuter l'opération sur chaque fichier ($f) avec une extension .txt.
Ensuite, do mv $f indique à bash de déplacer chacun de ces fichiers. Ensuite, vous devez fournir à bash le nouveau nom du fichier déplacé.
nouveau nom du fichier déplacé. Pour ce faire, vous devez supprimer le .txt et le remplacer par .md. Heureusement, bash dispose d'un opérateur intégré
pour supprimer la fin des chaînes de caractères. Vous pouvez utiliser l'opérateur %. Essayez cet exemple pour voir comment cela fonctionne
words="Hello World"
echo ${mots%Monde}
Vous pouvez maintenant ajouter quelque chose d'autre à la fin de la chaîne.
echo ${words%World)Moon
La syntaxe ${f%.txt}.md remplace donc toutes les chaînes .txt par des chaînes .md. Par ailleurs, si vous utilisez l'opérateur #
à la place du %, vous supprimerez une chaîne de caractères du début à la fin.
Vous devriez maintenant être capable de voir tous les nouveaux fichiers .wav dans le répertoire samples.
Dans votre terminal, tapez les commandes suivantes. Cela va convertir tous les fichiers .flac en fichiers .wav, puis supprimer les anciens fichiers.
les anciens fichiers.
for f in*.flac ; do ffmpeg -i "$f" "${f%.flac}.wav" ; done
rm*.flac
Cela prendra une minute ou deux, en fonction du modèle de Raspberry Pi que vous utilisez.

## Étape 3 Jouer des sons

Ensuite, vous allez commencer à écrire votre code Python. Vous pouvez utiliser n'importe quel éditeur de texte ou IDE pour ce faire - Mu est toujours un bon choix.

Pour commencer à créer les instruments de votre boîte à musique, vous devez tester si Python peut jouer certains des échantillons que vous avez copiés.

### Lecture de fichiers sonores avec Python

Pour lire un fichier son avec Python, vous pouvez utiliser un module appelé `pygame`. Ce module est normalement préinstallé sur un Raspberry Pi, mais en cas de problème vous pouvez ouvrir un terminal et taper :

```console
sudo pip3 install pygame
```

#### Importer et initialiser pygame

Vous devez d'abord importer le module pygame et l'initialiser.

``` python
import pygame
pygame.init()
```

#### Jouer un son

Ensuite, vous pouvez créer un objet Sound et lui fournir le chemin vers votre fichier.

``` python
mon_son = pygame.mixer.Sound('path/to/my/soundfile.wav')
```

Ensuite, vous pouvez jouer le son.

``` python
mon_son.play()
```

- Tout d'abord, importez et initialisez le module pygame pour la lecture des fichiers son.

``` python
import pygame
pygame.init()
```

- Enregistrez ce fichier dans votre répertoire `gpio-music-box`.
Choisissez quatre fichiers sonores que vous voulez utiliser pour votre projet, par exemple :

``` python
drum_tom_mid_hard.wav
drum_cymbal_hard.wav
drum_snare_hard.wav
drum_cowbell.wav
```

- Créez ensuite un objet Python qui renvoie à l'un de ces fichiers sonores. Donnez au fichier un nom unique. Par exemple :

``` python
drum = pygame.mixer.Sound("/home/pi/gpio-music-box/samplesdrum_tom_mid_hard.wav")
```

- Créez des objets nommés pour vos trois autres sons.

J'ai besoin d'un indice
Voici à quoi devrait ressembler votre code :

``` python
import pygame
pygame.init()
drum = pygame.mixer.Sound("/home/pi/gpio-music-box/samples/drum_tom_mid_hard.wav")
cymbal = pygame.mixer.Sound("/home/pi/gpio-music-box/samples/drum_cymbal_hard.wav")
snare = pygame.mixer.Sound("/home/pi/gpio-music-box/samples/drum_snare_hard.wav")
bell = pygame.mixer.Sound("/home/pi/gpio-music-box/samples/drum_cowbell.wav")
```

Si vous n'entendez aucun son, vérifiez que vos haut-parleurs ou votre casque fonctionnent et que le volume est augmenté.
Sauvegardez et exécutez votre code. Ensuite, dans le shell en bas de l'éditeur Mu, utilisez les commandes .play() pour jouer les sons.
les sons.
drum.play()

## Etape 4 Connecter vos boutons

Vous aurez besoin de quatre boutons, chacun étant relié à des broches GPIO distinctes sur le Raspberry Pi.

### Les broches GPIO du Raspberry Pi

GPIO est l'acronyme de General Purpose Input/Output (entrée/sortie à usage général). Un Raspberry Pi possède 26 broches GPIO. Celles-ci vous permettent d'envoyer et de recevoir des signaux de marche/arrêt vers et depuis des composants électroniques tels que des LED, des moteurs et des boutons.

Si vous regardez un Raspberry Pi avec les ports USB tournés vers vous, la disposition des broches GPIO est la suivante.

Chaque broche a un numéro, et il y a des broches supplémentaires qui fournissent des connexions de 3,3 volts, 5 volts et de masse.

Voici un diagramme montrant la disposition des broches :

![gpio](img/gpio-pinout.png)

Voici un tableau avec une brève explication.
| Abréviation  | Nom complet  | Fonction |
|---|---|---|
|3V3   | 3,3 volts   |Tout ce qui est connecté à ces broches est toujours alimenté en 3,3 V.   |
|5V   |  5 volts  |  Tout ce qui est connecté à ces broches sera toujours alimenté en 5V.  |
|GND   | Masse  |  Zéro volt, utilisé pour compléter un circuit.  |
|GP2   | Broche GPIO 2  | Ces broches sont destinées à un usage général et peuvent être configurées en entrée ou en sortie. |
|ID_SC/ID_SD/DNC    |   | Broches à usage spécial  |

### Utilisation d'un bouton avec un Raspberry Pi

Un bouton est l'un des composants d'entrée les plus simples que vous pouvez connecter à un Raspberry Pi. Il s'agit d'un composant non polarisé, ce qui signifie que vous pouvez le placer dans un circuit dans les deux sens et qu'il fonctionnera.

Il existe différents types de boutons : ils peuvent par exemple avoir deux ou quatre pattes. Les versions à deux pattes sont généralement utilisées avec un fil volant pour se connecter au dispositif de commande. Les boutons à quatre pattes sont généralement montés sur un circuit imprimé ou une
_breadboard_.

Les schémas ci-dessous montrent comment connecter un bouton à deux ou quatre pattes à un Raspberry Pi. Dans les deux cas, GPIO 17 est la
broche d'entrée.

![2 legs](img/gpio-2legs.png)

![4 legs](img/gpio-4legs.png)

Si vous utilisez plusieurs boutons, il est souvent préférable d'utiliser une masse commune pour éviter de connecter trop de fils de liaison aux broches GND. Vous pouvez relier le rail négatif de la planche d'essai à une seule broche de terre, ce qui permet à tous les boutons d'utiliser le même rail de terre.

![gnd](img/gpio-gnd.png)

!!! note "A faire"
    - Placez les quatre boutons sur votre _breadboard_.
    - Connectez chaque bouton à une broche GPIO numérotée différente. Vous pouvez choisir n'importe quelle broche mais vous devrez vous souvenir des numéros.

## Étape 5 Jouer des sons en appuyant sur un bouton

Pour voir comment une fonction peut être appelée en appuyant sur un bouton, consultez la section ci-dessous.
Déclenchement de fonctions à l'aide de boutons
Dans le schéma ci-dessous, un seul bouton a été câblé sur la broche 17.
Vous pouvez utiliser ce bouton pour appeler des fonctions qui ne prennent aucun argument :
Vous devez d'abord configurer le bouton en utilisant Python 3 et le module gpiozero.
from gpiozero import Button
btn = Bouton(17)
Ensuite, vous devez créer une fonction qui n'a pas d'arguments. Cette fonction simple va juste imprimer le mot Hello au shell.
le shell.
def hello() :
print('Hello')
Enfin, créez un déclencheur qui appelle la fonction.

```python
btn.when_pressed = hello
```

Maintenant, chaque fois que vous appuyez sur le bouton, vous devriez voir Hello s'afficher dans le shell Python.

Votre fonction peut être aussi complexe que vous le souhaitez - vous pouvez même appeler des fonctions qui font partie de modules. Dans cet exemple, l'appui sur le bouton allume une LED sur la broche 4.

```python
from gpiozero import Button, LED
btn = Bouton(17)
led = LED(4)
btn.when_pressed = led.on
```

Lorsque le bouton est pressé, le programme devrait appeler une fonction telle que `drum.play()`. Cependant, lorsque vous utilisez un événement (comme l'appui sur un bouton) pour appeler une fonction, vous n'utilisez pas de parenthèses ().
En effet, le programme ne doit appeler la fonction que lorsque le bouton est enfoncé, et non pas immédiatement. Ainsi, dans ce cas, vous utilisez simplement `drum.play`.

Liste complète du code

Tout d'abord, configurez l'un de vos boutons. N'oubliez pas d'utiliser les numéros des broches GPIO que vous avez utilisées, plutôt que ceux de l'exemple.

``` python
import pygame
from gpiozero import Button
pygame.init()
drum = pygame.mixer.Sound("/home/pi/gpio-music-box/samples/drum_tom_mid_hard.wav")
cymbale = pygame.mixer.Sound("/home/pi/gpio-music-box/samples/drum_cymbal_hard.wav")
caisse claire = pygame.mixer.Sound("/home/pi/gpio-music-box/samples/drum_snare_hard.wav")
bell = pygame.mixer.Sound("/home/pi/gpio-music-box/samples/drum_cowbell.wav")
btn_drum = Bouton(4)
1
2
3
4
5
6
7
8
9
10
11
```

Pour jouer le son lorsque le bouton est pressé, il suffit d'ajouter cette ligne de code au bas de votre fichier :

``` python
btn_drum.when_pressed = drum.play
```

Exécutez le programme et appuyez sur le bouton. Si vous n'entendez pas le son, vérifiez le câblage de votre bouton.

Maintenant, ajoutez du code pour que les trois autres boutons jouent leurs sons.

J'ai besoin d'un indice
Voici un exemple de code que vous pourriez utiliser pour un deuxième bouton.

``` python
btn_cymbal = Button(17)
btn_cymbal.when_pressed = cymbal.play
import pygame
from gpiozero import Button
pygame.init()
drum = pygame.mixer.Sound("/home/pi/gpio-music-box/samples/drum_tom_mid_hard.wav")
cymbal = pygame.mixer.Sound("/home/pi/gpio-music-box/samples/drum_cymbal_hard.wav")
snare = pygame.mixer.Sound("/home/pi/gpio-music-box/samples/drum_snare_hard.wav")
bell = pygame.mixer.Sound("/home/pi/gpio-music-box/samples/drum_cowbell.wav")
btn_drum = Button(4)
btn_cymbal = Button(17)
btn_snare= Button(27)
btn_bell = Button(10)
btn_drum.when_pressed = drum.play
btn_cymbal.when_pressed = cymbal.play
btn_snare.when_pressed = snare.play
btn_bell.when_pressed = bell.play
```

## Étape 6 Améliorer votre script

Le code que vous avez écrit devrait fonctionner sans problème. Cependant, c'est généralement une bonne idée de rendre votre code un peu plus propre une fois que vous avez un prototype qui fonctionne.

Les étapes suivantes sont totalement facultatives. Si vous êtes satisfait de votre script, laissez-le tel quel. Si vous voulez rendre
votre script un peu plus propre, suivez les étapes de cette page.

Vous pouvez stocker vos objets boutons et sons dans un dictionnaire, au lieu de devoir créer huit objets différents.

Consultez les étapes ci-dessous pour apprendre à créer des dictionnaires de base et à les parcourir en boucle.

### Dictionnaires en Python

Un dictionnaire est un type de structure de données en Python. Il contient une série de paires clé () : valeur (). Voici un exemple très simple

```python
band = {'john': 'rhythm guitar', 'paul': 'bass guitar', 'george': 'lead guitar', 'ringo': 'bass guitar'}
```

Le dictionnaire porte un nom, dans ce cas, `band`, et les données qu'il contient sont entourées de crochets (`{}`). À l'intérieur du dictionnaire se trouvent les paires clé () : valeur (). Dans ce cas, les clés sont les noms des membres du groupe. Les valeurs sont les noms des instruments qu'ils jouent. Les clés et les valeurs sont séparées par des deux-points ( :), et chaque paire est séparée par une virgule (,). par une virgule (,). Vous pouvez également écrire des dictionnaires de sorte que chaque paire clé () : valeur () soit écrite sur une nouvelle ligne.

``` python
groupe = {
    'john' : 'guitare rythmique',
    'paul' : 'guitare basse',
    'george' : 'lead guitar',
    'ringo' : 'guitare basse'.
}
```

Ouvrez IDLE, créez un nouveau fichier et essayez de créer votre propre dictionnaire. Vous pouvez utiliser le dictionnaire ci-dessus ou le vôtre si vous le souhaitez. Lorsque vous avez terminé, enregistrez et exécutez le code. Passez ensuite à l'interpréteur de commandes pour voir le résultat de l'opération en tapant le nom de votre dictionnaire.

Vous remarquerez probablement que les paires clé () : valeur () ne sont plus dans l'ordre dans lequel vous les avez tapées. Ceci est dû au fait que Cela s'explique par le fait que les dictionnaires Python ne sont pas ordonnés, et que vous ne pouvez pas compter sur une entrée particulière dans une position spécifique.
spécifique.
Pour rechercher une valeur () particulière dans un dictionnaire, vous pouvez utiliser sa clé (). Ainsi, par exemple, si vous souhaitez savoir quel instrument joue Ringo, vous pouvez taper :

``` python
band['ringo']
```

Les dictionnaires peuvent stocker tous les types de données. Vous pouvez donc les utiliser pour stocker des nombres, des chaînes de caractères, des variables, des listes ou même d'autres dictionnaires.

#### Boucler sur des dictionnaires avec Python

Comme toute structure de données en Python, vous pouvez itérer sur des dictionnaires.
N'oubliez pas que l'ordre des clés dans un dictionnaire peut être imprévisible.
Si vous itérez simplement sur un dictionnaire avec une boucle for, vous ne ferez qu'itérer sur les clés.
Prenons l'exemple de ce dictionnaire :

``` python
band = {
    'john' : 'rhythm guitar',
    'paul' : 'base guitar',
    'george' : 'lead guitar',
    'ringo' : 'bass guitar'
}
```

Vous pouvez itérer dessus avec une boucle for comme ceci :

``` python
for member in band :
    print(membre)
```

Ceci produira :

``` python
ringo
john
george
paul
```

Si vous voulez obtenir les clés et les valeurs, vous devrez spécifier ceci dans votre boucle for

``` python
for member, instrument in band.items() :
    print(membre, '-', instrument)
```

Cela donnera ce qui suit :

```python
ringo - guitare de base
john - guitare rythmique
george - guitare solo
paul - guitare de base
```

Liste complète du code

``` python
import pygame
from gpiozero import Button

pygame.init()

button_sounds = {Button(4): pygame.mixer.Sound("/home/pi/gpio-music-box/samples/drum_tom_mid_hard.wav"),
                 Button(17): pygame.mixer.Sound("/home/pi/gpio-music-box/samples/drum_cymbal_hard.wav"),
                 Button(27): pygame.mixer.Sound("/home/pi/gpio-music-box/samples/drum_snare_hard.wav"),
                 Button(10): pygame.mixer.Sound("/home/pi/gpio-music-box/samples/drum_cowbell.wav")}

for button, sound in button_sounds.items():
    button.when_pressed = sound.play
```

Vous pouvez maintenant boucler sur le dictionnaire pour indiquer au programme de jouer le son lorsque le bouton est pressé :

``` python
for button, sound in button_sounds.items():
    button.when_pressed = sound.play
```

Défi : une discothèque mobile
Pouvez-vous utiliser différents sons dans votre programme ?
Pouvez-vous ajouter d'autres boutons pour obtenir une plus grande variété d'instruments ?
Pouvez-vous ajouter des LED à votre projet et les faire clignoter lorsqu'un son est joué ?
Défi !

## Étape 7 Réflexion

Félicitations pour avoir terminé le projet de boîte à musique GPIO. Nous aimerions savoir si vous pensez que ce projet vous a aidé à développer vos compétences en matière de fabrication numérique.

## Étape 8 Et ensuite ?

Essayez d'autres projets d'informatique physique.
