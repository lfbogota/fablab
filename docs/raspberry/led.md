# Allumer et éteindre des LEDs

## Premier montage

Le but de la première partie du TP est de commander l’allumage d’une DEL à l’aide du Raspberry Pi. La tension aux bornes de la DEL ne devant pas dépasser 1,5 V , on utilise une résistance de protection de valeur 1 kΩ afin de limiter la tension aux bornes de la DEL. L’allumage de la DEL sera commandé par la broche 17 du connecteur GPIO.

!!!note "A faire"
    - Réalisez le montage suivant à l’aide du matériel à votre disposition.

    ![schema](img/led-1-schema.png)

## Controle avec Python

Nous allons interagir avec le connecteur GPIO à l’aide du language de programmation Python et de la librairie `gpiozero`.

- Cliquez sur le menu en haut à gauche et lancer l’éditeur _Python 3 (IDLE)_ à partir du sous-menu _Programming_.
- Copiez le code ci-dessous et enregistrez le fichier avec le nom `led-1.py`.

    ```python
    from gpiozero import LED
    from time import sleep
    led = LED (17) # on utilise la branche GPIO 17
    led.on()  # allumer la del
    sleep(1)  # attendre 1 seconde
    led.off()  # eteindre la del
    ```

- Lancer l’exécution du programme à l’aide de la touche <kbd>F5</kbd>. Vous devriez voir la lampe s’allumer puis s’éteindre.
- Vérifier le câblage du montage et le sens de branchement de la DEL si ce n’est pas le cas.

!!! note "A faire"
    Ajouter une boucle `#!python for` au programme précédent pour que la lampe s’allume et s’éteigne 10 fois de suite.

## Interface Web

### Serveur Flask

Flask est une librairie Python permettant de réaliser des sites web dynamiques. Flask déploie un serveur web qui permet d’analyser les requêtes HTTP envoyées par des clients à l’aide de leur navigateur web. En fonction de la requête reçue, une commande est envoyée au connecteur GPIO afin d’allumer ou d’éteindre la lampe. Une réponse HTTP contenant un message de confirmation est ensuite renvoyée au client.

!!! note "A faire"

    - Créer un fichier `helloworld.py` à l’aide d’IDLE.
    - Copier le code ci dessous dans le fichier.

        ``` python
        from flask import Flask
        app = Flask(__name__)

        @app.route("/")
        def hello():
            return " Hello World ! "

        if __name__ == "__main__":
            app.run(debug=True)
        ```

    - Exécuter le programme et ouvrir l’adresse <http://127.0.0.1:5000/> dans le navigateur. Le message _Hello World_ devrait être visible. L’adresse 127.0.0.1 correspond à l’adresse de l’hôte local (_localhost_) et 5000 est le numéro de port utilisé. On peut aussi utiliser l'adresse <http://localhost:5000/> pour accéder à notre site.
    - L’option `debug=True` permet de ne pas avoir à relancer le programme lorsque le code est modifié. Modifier le fichier `helloworld.py` pour que le message affiché soit Commander la DEL à distance.
    - Recharger la page dans le navigateur et vérifier les changements.
    - Arrêter le programme avec la touche <kbd>Ctrl</kbd> + <kbd>C</kbd>.

### Accès depuis une autre machine

Dans l’activité précédente le client envoyant la requête se trouve sur la même machine que le serveur. Nous allons maintenant modifier notre programme pour qu’il accepte les requêtes des clients se trouvant sur le réseau local.

!!!note "A faire"
    - Modifier le programme précédent en ajoutant l’option `host='0.0.0.0'` à la dernière ligne afin de rendre notre site accessible depuis une autre machine du réseau local.

        ``` python hl_lines="9"
        from flask import Flask
        app = Flask(__name__)

        @app.route("/")
        def hello():
            return " Hello World ! "

        if __name__ == "__main__":
            app.run(debug=True, host='0.0.0.0')
        ```

    - Pour accéder à votre site web, le client doit connaitre l’adresse IP de votre machine. Ouvrir un terminal et entrer la commande `ifconfig`.
    Dans l’exemple ci-dessus, l’interface réseau est la carte WiFi et est repérée par l’identifiant `wlan0`. L’adresse IP de la machine est `192.168.0.20`.
    - Depuis le poste d’un de vos camarades ou depuis votre téléphone (si il est sur le réseau du lycée), tapez l’adresse IP suivie du port (<http://192.168.0.20:5000/> pour l’exemple précédent) et vérifiez que vous pouvez accéder à votre site.

## Interactions avec le connecteur GPIO

Nous allons maintenant intégrer les instructions permettant d’allumer la lampe depuis notre application web.

!!!note "A faire"
    - Créer un ficher `led-on-off.py` et copier le code ci-dessous :

        ``` python
        from flask import Flask
        from gpiozero import LED

        led = LED(17)
        app = Flask(__name__)

        @app.route("/on/")
        def on():
            led.on()
            return "Lampe allumee"

        if __name__ == "__main__":
            app.run(debug=True, host="0.0.0.0")
        ```

    - Dans le navigateur, ouvrir la page <http://localhost:5000/on/> et vérifier que la DEL s’allume.
    - Modifier le programme en ajoutant une route `/off/` qui commande l’extinction de la DEL lorsque l’on accède à la page     <http://localhost:5000/off/>.

### Ajout de liens

On souhaite rajouter des liens hypertexte pour passer d’une page à l’autre. La page `/on/` devra contenir un lien permettant de passer à la page `/off/` et inversement.

``` python
@app.route("/on/")
def on():
    led.on()
    return "La lampe est allumée <a href='/off/'>éteindre</a>"
```

!!! note "A faire"
    - Modifier le programme pour pouvoir passer d’une page à l’autre tout en allumant et éteignant la DEL.

### Gabarit ou template

``` python hl_lines="1 10"
from flask import Flask, render_template
from gpiozero import LED

led = LED(17)
app = Flask(__name__)

@app.route("/on/")
def on():
    led.on()
    return render_template("on.html")   

@app.route("/off/")
def off():
    led.off()
    return "Lampe éteinte <a href='/on/'>allumer</a>"

if __name__ == "__main__":
    app.run(debug=True, port=5000, host="0.0.0.0")
```

Fichier `on.html` :

``` html
<html lang="fr">
    <head>
        <meta charset="utf-8" />
        <title>Lampe allumée</title>
    </head>
    <body>
        <h1>La lampe est allumée</h1>
        <a href="/off/">éteindre</a>
    </body>
</html>
```

### Style

On utilise les classes de la librairie bulma.css

```html hl_lines="4 8 9"
<html lang="fr">
    <head>
        <meta charset="utf-8" />
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.9.3/css/bulma.min.css" />
        <title>Lampe allumée</title>
    </head>
    <body>
        <h1 class="title is-1">La lampe est allumée</h1>
        <a class="button" href="/off/">Eteindre</a>
    </body>
</html>
```
