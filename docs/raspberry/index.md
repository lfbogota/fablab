# Sommaire

1. [Prise en main du Raspberry Pi](intro.md)
1. [Utiliser la caméra du Raspberry Pi](camera.md)
1. [Réaliser une photographie time-lapse](timelapse.md)
1. [Réaliser une vidéo en stop motion](stopmotion.md)
1. [Programmer un jeu de réaction rapide](reaction.md)
1. [Fabriquer une boite à musique](musicbox.md)
1. Fabriquer un détecteur de mouvement
1. [Commander l'allumage de LEDs](led.md)
1. Mesurer une température
1. Controler un servo moteur
