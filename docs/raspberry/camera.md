# Utiliser la caméra du Raspberry Pi

## Prise en main

### Module caméra

Le module caméra va nous permettre de prendre des photos ou des vidéos avec notre Raspberry Pi. Il peut être controlé par un programme en Python ou par la ligne de commande.

![enter image description here](https://projects-static.raspberrypi.org/projects/getting-started-with-picamera/659a837aafb1ff69f222c16debae07be88459ab3/en/images/camera-module.png)

### Port

Tous les modèles récents de raspberry possèdent un port pour brancher le module caméra.

![Port de la caméra](https://projects-static.raspberrypi.org/projects/getting-started-with-picamera/659a837aafb1ff69f222c16debae07be88459ab3/en/images/pi4-camera-port.png)

### Branchement

!!!note "A faire"
    - Vérifier que le Raspberry Pi est éteint.
    - Repérer le port du module caméra sur votre raspberry à l'aide de l'image ci-dessus.
    - Tirez doucement sur les bords du clip en plastique du port.
    - Insérez le câble plat du module caméra ; assurez-vous que les connecteurs situés en bas du câble plat sont en face des contacts du port.
    - Repoussez le clip en plastique en place.

    ![Branchement du module caméra](https://projects-static.raspberrypi.org/projects/getting-started-with-picamera/659a837aafb1ff69f222c16debae07be88459ab3/en/images/connect-camera.gif)

    - Démarrer le Raspberry Pi.
    - Ouvrir l'outil de configuration du Raspberry Pi dans le menu Principal

    ![Outil de configuration du Raspberry Pi](https://projects-static.raspberrypi.org/projects/getting-started-with-picamera/659a837aafb1ff69f222c16debae07be88459ab3/en/images/pi-configuration-menu.png)
    
    - Cocher _Enabled_ dans l'onglet _Interfaces_ pour activer la caméra :

    ![Activation de la caméra](https://projects-static.raspberrypi.org/projects/getting-started-with-picamera/659a837aafb1ff69f222c16debae07be88459ab3/en/images/pi-configuration-interfaces-annotated.png)

    - Redémarrer votre Raspberry Pi.

## Controler la caméra avec la ligne de commande

Maintenant que votre module caméra est connecté et que le logiciel est activé, essayez les outils de ligne de commande `raspistill` et `raspivid`.

- Ouvrez une fenêtre de terminal en cliquant sur l'icône de moniteur noir dans la barre des tâches :
    ![Ouvrir le terminal](img/open-terminal-annotated.png)

- Tapez la commande suivante pour prendre une image fixe et l'enregistrer sur le Bureau :

    ```bash
    raspistill -o Desktop/image.jpg
    ```

    ![Commande raspistill saisie dans le terminal](img/raspistill-image.png)

- Appuyez sur Entrée pour exécuter la commande.

Lorsque la commande s'exécute, vous pouvez voir l'aperçu de la caméra s'ouvrir pendant cinq secondes avant qu'une photo ne soit prise.

- Recherchez l'icône du fichier image sur le Bureau, et double-cliquez sur l'icône du fichier pour ouvrir l'image.

![Image sur le bureau](img/desktop-file.png)

En ajoutant différentes options, vous pouvez définir la taille et l'apparence de l'image prise par la commande `raspistill`.

- Par exemple, ajoutez `-h` et `-w` pour modifier la hauteur et la largeur de l'image :

    ```bash
    raspistill -o Desktop/image-small.jpg -w 640 -h 480
    ```

- Enregistrez maintenant une vidéo avec le module caméra en utilisant la commande `raspivid` suivante :

    ```bash
    raspivid -o Desktop/video.h264
    ```

- Pour lire le fichier vidéo, double-cliquez sur l'icône du fichier `video.h264` sur le bureau pour l'ouvrir dans VLC Media Player.

## Controler la caméra avec un programme Python

La librairie `picamera` vous permet de controler le module caméra depuis un programme écrit dans le language Python.

- Ouvrez un éditeur Python comme IDLE.

- Ouvrez un nouveau fichier et enregistrez-le sous le nom de `camera.py`.

    !!!tip "Remarque"
        Il est important de ne jamais appeler votre fichier `picamera.py` afin de ne pas avoir d'erreur lors de l'importation du module `picamera`.

- Entrez le code suivant :

    ``` python
    from picamera import PiCamera
    from time import sleep

    camera = PiCamera()

    camera.start_preview()
    sleep(5)
    camera.stop_preview()
    ```

- Sauvegardez et exécutez votre programme. L'aperçu de la caméra doit s'afficher pendant cinq secondes, puis se refermer.

    !!!tip "Remarque"
        Si l'apercu est à l'envers, vous pouvez le renverser avec la ligne de code suivante :

        ``` python
        camera = PiCamera()
        camera.rotation = 180
        ```

        Vous pouvez faire pivoter l'image de 90, 180 ou 270 degrés. Réglez la rotation sur 0 degré pour réinitialiser l'image.

    !!!tip "Remarque"
        Il est préférable de rendre l'aperçu légèrement transparent afin que vous puissiez voir si des erreurs se produisent dans votre programme lorsque l'aperçu est activé.

        Rendez l'aperçu de la caméra transparent en définissant un niveau alpha :

        ``` python
        camera.start_preview(alpha=200)
        ```

        La valeur du paramètre alpha doit être un entier compris entre 0 et 255.

### Prendre des photos

Nous allons maintenant utiliser le module caméra et Python pour prendre des photos.

!!!note "A faire"
    Modifiez votre code pour ajouter une ligne `camera.capture()` :

    ``` python
    camera.start_preview()
    sleep(5)
    camera.capture('/home/pi/Desktop/image.jpg')
    camera.stop_preview()
    ```

!!! tip "Remarque"
    Il est important d'attendre au moins deux secondes avant de capturer une image car cela donne au capteur de la caméra le temps de détecter les niveaux de lumière.

- Exécutez le code.

- Vous devriez voir l'aperçu de la caméra s'ouvrir pendant cinq secondes, puis une image fixe devrait être capturée. Pendant que la photo est prise, vous pouvez voir l'aperçu s'ajuster brièvement à une résolution différente.

- Votre nouvelle image devrait être enregistrée sur le Bureau.

- Ajoutez maintenant une boucle pour prendre cinq photos à la suite :

    ``` python
    camera.start_preview()
    for i in range(5) :
        sleep(5)
        camera.capture('/home/pi/Desktop/image%s.jpg' % i)
    camera.stop_preview()
    ```

- La variable `i` compte le nombre de fois que la boucle a été exécutée, de 0 à 4. Par conséquent, les images sont enregistrées sous le nom `image0.jpg`, `image1.jpg`, et ainsi de suite.

- Exécutez à nouveau le code et maintenez le module caméra en position.

- La caméra va prendre une photo toutes les cinq secondes. Une fois la cinquième photo prise, l'aperçu se ferme.

- Regarder sur le bureau du Raspberry Pi pour trouver les cinq images.

### Enregistrer une vidéo

Maintenant, enregistrons une vidéo !

- Modifier votre code pour supprimer `capture()` et ajouter `start_recording()` et `stop_recording()` à la place.

    ``` python hl_lines="2 4"
    camera.start_preview()
    camera.start_recording('/home/pi/Desktop/video.h264')
    sleep(5)
    camera.stop_recording()
    camera.stop_preview()
    ```

- Exécuter le code.
    Votre Raspberry Pi devrait ouvrir un aperçu, enregistrer 5 secondes de vidéo, puis fermer l'aperçu.
- Ouvrir le fichier sur le bureau pour vérifier que l'enregistrement a fonctionné.

## Paramètres de l'image et ajout d'effets d'image

Le logiciel Python `picamera` offre un certain nombre d'effets et de configurations pour modifier l'aspect de vos images.

!!!tip "Remarque"
    Certains paramètres n'affectent que l'aperçu et non l'image capturée, d'autres n'affectent que l'image capturée, et beaucoup d'autres encore affectent les deux.

### Définir la résolution de l'image

Vous pouvez modifier la résolution de l'image prise par le module caméra.

Par défaut, la résolution de l'image est réglée sur la résolution de votre écran. La résolution maximale est de 2592×1944 pour les photos et de 1920×1080 pour les enregistrements vidéo.

Utilisez le code suivant pour régler la résolution au maximum et prendre une photo.

!!! tip "Remarque"
    Vous devez également régler la fréquence d'images sur 15 pour activer cette résolution maximale.

    ``` python hl_lines="2"
    camera.resolution = (2592, 1944)
    camera.framerate = 15
    camera.start_preview()
    sleep(5)
    camera.capture('/home/pi/Desktop/max.jpg')
    camera.stop_preview()
    ```

La résolution minimale est de 64×64.

Essayez de prendre une photo avec la résolution minimale.

### Ajouter du texte à votre image

Vous pouvez ajouter du texte à votre image en utilisant la `commande annotate_text`.

- Exécutez ce code pour l'essayer :

    ``` python
    camera.start_preview()
    camera.annotate_text = "Bonjour le monde !"
    sleep(5)
    camera.capture('/home/pi/Desktop/text.jpg')
    camera.stop_preview()
    ```

### Modifier l'apparence du texte ajouté

- Définissez la taille du texte avec le code suivant :

    ``` python
    camera.annotate_text_size = 50
    ```

    Vous pouvez définir la taille du texte entre 6 et 160. La taille par défaut est de 32.

- Il est également possible de modifier la couleur du texte.

    Tout d'abord, ajoutez `Color` à votre ligne d'importation en haut du programme :

    ``` python
    from picamera import PiCamera, Color
    ```

    Ensuite, sous la ligne d'importation, modifiez le reste de votre code pour qu'il ressemble à ceci :

    ``` python
    camera.start_preview()
    camera.annotate_background = Color('blue')
    camera.annotate_foreground = Couleur('jaune')
    camera.annotate_text = "Hello world!"
    sleep(5)
    camera.stop_preview()
    ```

### Modifier la luminosité de l'aperçu

Vous pouvez modifier la luminosité de l'aperçu avec l'instruction `camera.brightness`. La luminosité par défaut est de 50 et vous pouvez la régler sur une valeur comprise entre 0 et 100.

- Exécutez le code suivant pour tester le changement de luminosité :

    ``` python hl_lines="2"
    camera.start_preview()
    camera.brightness = 70
    sleep(5)
    camera.capture('/home/pi/Desktop/bright.jpg')
    camera.stop_preview()
    ```

- La boucle `for` ci-dessous fait varier la luminosité et ajoute également du texte pour afficher la valeur actuelle :

    ``` python
    camera.start_preview()
    for i in range(100):
        camera.annotate_text = "Luminosité : %s" % i
        camera.brightness = i
        sleep(0.1)
    camera.stop_preview()
    ```

### Modifier le contraste de l'aperçu

Comme pour la luminosité de l'aperçu, vous pouvez modifier le contraste de l'aperçu.

- Exécutez le code suivant pour faire varier la valeur du contraste :

    ``` python
    camera.start_preview()
    for i in range(100):
        camera.annotate_text = "Contraste : %s" % i
        camera.contrast = i
        sleep(0.1)
    camera.stop_preview()
    ```

### Ajouter des effets à l'image

Vous pouvez utiliser `camera.image_effect` pour appliquer un effet à l'image.

Les options d'effet d'image sont :

- `none`
- `solarize`
- `sketch`
- `denoise`
- `emboss`
- `oilpaint`
- `hatch`
- `gpen`
- `pastel`
- `watercolor`
- `film`
- `blur`
- `saturation`
- `colorswap`
- `washedout`
- `posterise`
- `colorpoint`
- `colorbalance`
- `cartoon`
- `deinterlace1`
- `deinterlace2`

L'effet par défaut est `none`.

Choisissez un effet d'image et essayez-le :

``` python hl_lines="2"
camera.start_preview()
camera.image_effect = 'colorswap'
sleep(5)
camera.capture('/home/pi/Desktop/colorswap.jpg')
camera.stop_preview()
```

!!!tip "Remarque"
    Il est possible d'utiliser une boucle `for` pour tester tous les effets d'image contenus dans `camera.IMAGE_EFFECTS` :

    ``` python hl_lines="2"
    camera.start_preview()
    for effect in camera.IMAGE_EFFECTS:
        camera.image_effect = effect
        camera.annotate_text = "Effet: %s" % effect
        sleep(5)
    camera.stop_preview()
    ```

## Crédits et licence

Ce document est une adaptation du tutoriel officiel [Getting started with picamera](https://projects.raspberrypi.org/en/projects/getting-started-with-picamera/) de la [fondation Raspberry Pi](https://www.raspberrypi.org/) disponible sous licence Creative Commons [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.fr).
