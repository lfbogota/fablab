
# Réaliser une photographie time-lapse

## Introduction

Dans ce projet vous allez écrire un petit programme en Python pour capturer à l'aide d'une caméra reliée à un Raspberry Pi plusieurs images sur une longue période de temps.

Ces images peuvent ensuite être combinées dans un GIF animé, vous permettant de visualiser des événements très lents en quelques secondes.

## Matériel

!!! abstract "Ce dont vous avez besoin"
    - Hardware
        - Module caméra Raspberry Pi

    - Software
        - Ce projet nécessite ImageMagick, un programme en ligne de commande pour la manipulation d'images. Pour installer ImageMagick, exécutez les commandes suivantes dans une fenêtre de terminal :
        ```
        sudo apt-get update
        sudo apt-get install imagemagick -y
        ```

## Photographies time-lapse avec un Raspberry Pi

La photographie time-lapse utilise plusieurs images prises sur une longue période, qui sont ensuite assemblées pour produire une séquence d'images animées.

Si vous n'avez jamais vu de time-lapse auparavant, l'image ci-dessous est un exemple de ce qui peut être réalisé.

![haricot mungo](<https://projects-static.raspberrypi.org/projects/timelapse-setup/7c9393eb89f3de971afd5fe0d0c3ff92df210cea/en/images/mungbeans.gif>)

Si vous n'avez jamais utilisé le module caméra de Raspberry Pi, nous vous conseillons de parcourir rapidement les premières étapes du projet [Utiliser la caméra du Raspberry Pi](../raspberry/camera.md) pour vous familiariser avec le dispositif et vous assurer qu'il fonctionne correctement.

## Prendre une photo

Vous pouvez commencer par écrire un script simple pour prendre une photo à l'aide du module caméra.

- On utilise IDLE pour écrire notre programme python : <kbd>Menu</kbd> > <kbd>Programmation</kbd> > <kbd>Python 3 (IDLE)</kbd>.

- Créez un nouveau fichier (<kbd>Fichier</kbd> > <kbd>Nouveau fichier</kbd>), puis enregistrez-le sous le nom de `timelapse.py`.

- Maintenant, avec trois simples lignes de code, vous pouvez utiliser Python pour prendre une photo :

    ``` python
    from picamera import PiCamera

    camera = PiCamera()

    camera.capture('image.jpg')
    ```

- Enregistrez votre script ( <kbd>Ctrl</kbd> +<kbd>S</kbd> ), puis exécutez-le en appuyant sur la touche <kbd>F5</kbd> de votre clavier.

- Ouvrez le gestionnaire de fichiers en cliquant sur l'icône du gestionnaire de fichiers en haut à gauche de l'écran, et double-cliquez sur `image.jpg` :

## Prendre plusieurs images

Vous pouvez prendre plusieurs images à l'aide du module caméra en capturant des images avec une boucle. Une boucle `for` peut être utilisée pour capturer un nombre défini d'images.

Modifiez votre fichier pour incorporer une boucle `for`. Dans cet exemple, la caméra Pi va capturer 10 images :

``` python
from picamera import PiCamera

camera = PiCamera()

for i in range(10) :
    camera.capture('image.jpg')
```

Sauvegardez à nouveau votre script et exécutez votre programme ( <kbd>F5</kbd> ). Jetez ensuite un coup d'œil dans votre gestionnaire de fichiers pour voir ce qui a été créé.

Pouvez-vous voir le problème ? Il n'y a qu'une seule image, et c'est la dernière qui a été prise. Cela s'explique par le fait que chaque image avait le même nom de fichier et qu'elle a été écrasée par l'image suivante. C'est un problème qui peut être résolu par une petite modification du script :

``` python hl_lines="6"
from picamera import PiCamera

camera = PiCamera()

for i in range(10) :
    camera.capture('image{0:04d}.jpg'.format(i))
```

Si vous regardez maintenant dans le gestionnaire de fichiers, vous devriez voir dix images nommées `image0000.jpg` jusqu'à `image0009.jpg`.

La syntaxe `'image{0:04d}.jpg'.format(i)` ajoute la valeur de `i`, qui commence à 0 et se termine à 9, au nom du fichier. Elle ajoute également des zéros au nombre, de sorte qu'il y ait toujours 4 chiffres. Ce point sera important par la suite.

## Création d'un GIF

Maintenant que vous savez comment prendre plusieurs photos, voyons comment transformer cette séquence en un GIF animé. Pour cela, vous aurez besoin du programme _ImageMagick_.

_ImageMagick_ est un programme en ligne de commande qui peut être utilisé pour manipuler des images. Pour tester son fonctionnement, vous pouvez ouvrir votre terminal ( <kbd>Ctrl+Alt+t</kbd> ) et taper la commande suivante :

``` console
convert -delay 10 -loop 0 image*.jpg animation.gif
```

L'option `-delay` définit le temps (en centièmes de seconde) entre les images. L'option `-loop` définit le nombre de boucles du GIF. Ici, la valeur `0` indique que la boucle sera permanente.

L'exécution prendra un peu de temps, mais une fois terminée, vous devriez voir le fichier `animation.gif` dans le gestionnaire de fichiers :

animé

Vous pouvez double-cliquer sur ce fichier et regarder l'animation dans _Image Viewer_. Là encore, laissez-lui un peu de temps pour s'ouvrir, car il s'agit probablement d'un fichier assez volumineux.

Comme pour tous les programmes en ligne de commande, vous pouvez exécuter _ImageMagick_ à partir de Python. Il vous suffit d'utiliser la fonction `system` de la bibliothèque `os` :

``` python hl_lines="9"
from picamera import PiCamera
from os import system

camera = PiCamera()

for i in range(10) :
    camera.capture('image{0:04d}.jpg'.format(i))

system('convert -delay 10 -loop 0 image*.jpg animation.gif')
print('done')
```

L'exécution de cette opération va prendre un peu de temps. Vous devriez voir le mot "done" imprimé dans le shell lorsque le script sera terminé. Votre nouveau fichier `animation.gif` sera lisible à partir du gestionnaire de fichiers après quelques minutes.

## Réduire la taille du fichier

À l'heure actuelle, votre GIF animé est probablement de l'ordre de 10 Mo, ce qui est un peu lourd pour une animation de dix images. En effet, la caméra de votre Pi capture des images à une résolution de 3280 x 2464 si vous avez un module caméra v2, ou de 1920 x 1080 si vous avez un module caméra original. Vous pouvez produire des GIFs plus petits en utilisant une résolution d'image plus faible.

Retournez à votre fichier `timelapse.py`. Ajoutez maintenant une nouvelle ligne pour définir la résolution des images :

``` python hl_lines="5"
from picamera import PiCamera
from os import system

camera = PiCamera()
camera.resolution = (1024, 768)

for i in range(10) :
    camera.capture('image{0:04d}.jpg'.format(i))

system('convert -delay 10 -loop 0 image*.jpg animation.gif')
```

Si vous voulez des GIF encore plus petits, choisissez une résolution encore plus faible.

## Ajout d'un délai

L'intérêt du time-lapse est de prendre des photos toutes les quelques minutes, voire toutes les heures. Pour ce faire, vous pouvez mettre votre programme en pause entre les captures, en utilisant la bibliothèque `time`.

- De retour dans votre fichier `timelapse.py`, modifiez le code afin de pouvoir importer la fonction sleep, puis mettez le script en pause après chaque capture :

``` python hl_lines="3 10"
from picamera import PiCamera
from os import system
from time import sleep

camera = PiCamera()
camera.resolution = (1024, 768)

for i in range(10) :
    camera.capture('image{0:04d}.jpg'.format(i))
    sleep(60)

system('convert -delay 10 -loop 0 image*.jpg animation.gif')
```

- Dans l'exemple ci-dessus, une photo est prise toutes les 60 secondes, et dix photos sont prises au total. Vous pouvez maintenant modifier les valeurs des fonctions `range()` et `sleep()` en fonction de vos besoins. Si vous voulez capturer l'ouverture d'une fleur, une photo par minute pendant quelques heures suffira. Si vous souhaitez réaliser un time-lapse d'un fruit en train de pourrir, deux ou trois images par jour seront plus appropriées.

- Installez maintenant votre Raspberry Pi avec le module caméra dirigé vers votre sujet, exécutez le script, puis attendez que le GIF soit créé.

## Crédits et licence

Ce document est une adaptation du tutoriel officiel [Time-lapse animations with a Raspberry Pi](https://projects.raspberrypi.org/en/projects/timelapse-setup/) de la [fondation Raspberry Pi](https://www.raspberrypi.org/) disponible sous licence Creative Commons [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.fr).
