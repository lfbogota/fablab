# Détecteur de mouvement

[https://projects.raspberrypi.org/en/projects/laser-tripwire](https://projects.raspberrypi.org/en/projects/laser-tripwire)

## Introduction

Dans ce projet, vous allez utiliser un pointeur laser, quelques composants électroniques et un Raspberry Pi pour fabriquer un détecteur de mouvement. Chaque fois qu'un intrus coupera le faisceau laser, le Raspberry Pi déclenchera l'alarme via un buzzer ou un haut-parleur.

En réalisant ce projet, vous aller apprendre à :

- mesurer les niveaux de lumière avec une photorésistance (_light dependant resistor_ ou LDR en anglais)
- contrôler un buzzer
- jouer des sons à l'aide du module Python `pygame`
- écrire et appeler des fonctions lambda simples

## Ce dont vous aurez besoin

!!!abstract "Matériel"
    - un Raspberry Pi
    - une _breadboard_
    - un condensateur de 1µF
    - une résistance dont la valeur dépend de l'éclairement (photorésistance)
    - 3 fils de liaison femelle-mâle (pour les tests)
    - 3 fils de liaison femelle-femelle
    - un pointeur laser
    - une paille

## Détection d'un faisceau cassé

Votre première étape consistera à créer un prototype simple sur une planche à pain pour détecter si un faisceau de lumière frappe ou non la résistance dépendant de la lumière (photorésistance).

### Blindage de la photorésistance

C'est une bonne idée de protéger votre photorésistance pour vous assurer que seule la lumière du pointeur laser déclenchera votre programme. Pour ce faire, vous pouvez utiliser un petit rouleau de papier ou, plus facilement encore, un morceau de paille.

![ldr](img/ldr-straw-1.jpeg)
![ldr](img/ldr-straw-2.jpeg)

### Construction et codage de votre capteur de lumière

Vous allez maintenant devoir faire plusieurs choses :

- Configurer votre circuit en utilisant un condensateur et une photorésistance reliés à votre Raspberry Pi. Il y a une section optionnelle ci-dessous qui explique comment les circuits de temporisation RC fonctionnent. L'autre section explique comment utiliser Python pour détecter les niveaux de lumière.
Théorie du chronométrage RC

#### Utilisation d'une photorésistance pour mesurer la lumière

Dans cette ressource, vous apprendrez à construire un circuit utilisant une photorésistance et à écrire un script sur le Raspberry Pi pour détecter la lumière.

![ldr](img/ldr.webp)

La valeur de la résistance d'une photorésistance change en fonction de la quantité de lumière qui la frappe. Plus la lumière frappe la photorésistance, plus sa résistance est faible. Si vous êtes intéressé par la physique de ce fonctionnement, vous pouvez consulter cette ressource.

Les broches GPIO du Raspberry Pi peuvent être utilisées avec une grande variété de composants électroniques. Certains composants sont analogiques, d'autres sont numériques. Les composants numériques sont très faciles à utiliser avec un Pi, car les broches GPIO du Pi sont uniquement capables d'être allumées ou éteintes, et de détecter si un autre appareil est allumé ou éteint. Pour utiliser des composants analogiques, par exemple des photorésistances, vous devez faire un peu plus d'efforts.

#### Montage du circuit

Vous allez réaliser un circuit résistance-condensateur (circuit RC) à l'aide d'un condensateur de 1µF et d'une photorésistance. Les deux composants devront être en série l'un avec l'autre. Une branche de la photorésistance sera reliée à une broche de 3,3 V (étiquetée 3V3) du Raspberry Pi. La branche négative du condensateur sera reliée à une broche de mise à la terre (étiquetée GND). La branche positive du condensateur sera connectée à l'une des broches GPIO standard du Raspberry Pi. Voici une configuration possible du circuit.

#### Le script sur le Raspberry Pi

Le code pour le capteur de lumière est assez simple. Il vous fournira une valeur relative de la lumière dans l'environnement sous la forme d'un nombre à virgule flottante entre 0 et 1. Plus la valeur est élevée, plus la lumière tombe sur le LDR. Dans l'exemple de code ci-dessous, on suppose que la LDR a été connectée au GPIO 17.

``` python
from gpiozero import LightSensor
ldr = LightSensor(17)
print(ldr.value)
```

Autres méthodes
La classe LightSensor possède les autres méthodes suivantes :

```python
## Pause jusqu'à ce qu'aucune lumière ne soit détectée
wait_for_dark()

## Pause jusqu'à ce que la lumière soit détectée
wait_for_light()

## Retourne True si la lumière est détectée
light_detected

## Fonction à exécuter lorsqu'il fait sombre
when_dark = my_function

## Fonction à exécuter lorsqu'il fait jour
when_light = ma_fonction
```

Vous êtes maintenant prêt à écrire un script pour détecter quand le faisceau laser est interrompu. Vous pouvez utiliser les méthodes du module `gpiozero` pour le faire. L'une de vos options est d'utiliser une boucle `while True` contenant la méthode `wait_for_dark`. Toutefois, il est préférable d'utiliser la méthode `when_dark`. Si vous choisissez cette méthode, vous devez soit créer une fonction pour imprimer 'INTRUS', soit utiliser une fonction lambda.

### Expressions lambda en Python

L'utilisation des expressions lambda est une méthode simple pour créer des fonctions en Python en une seule ligne. Bien qu'elles ne soient pas indispensables à votre programmation, les expressions lambda peuvent parfois s'avérer très utiles. Elles sont particulièrement utiles lorsque vous souhaitez utiliser une fonction comme argument dans une autre fonction.

Voici un exemple simple. Imaginez que vous souhaitiez créer une fonction qui prend un nombre et lui ajoute 1. Vous pourriez écrire ce qui suit :

``` python
def add_one(valeur) :
    retourne valeur + 1

answer = add_one(100)
```

Ceci pourrait être réécrit en utilisant une expression lambda :

``` python
add_one = lambda valeur : valeur + 1
answer = add_one(100)
```

La syntaxe générale d'une expression lambda consiste à utiliser le mot-clé lambda suivi des paramètres de la fonction, puis des deux points (`:`). Après les deux-points viennent les valeurs à renvoyer par la fonction. Par exemple, cette expression lambda permet d'additionner deux nombres :

```python
add_two = lambda a, b : a + b
add_two(10, 5)
```

Vous pouvez écrire des expressions lambda plus complexes, et même ajouter des déclarations conditionnelles dans la valeur de retour. Cependant, il est préférable de garder les lambdas simples, pour s'assurer qu'elles restent lisibles.

Si vous avez besoin d'un peu d'aide, jetez un coup d'œil aux conseils ci-dessous.

J'ai besoin d'une astuce
Importez la classe LightSensor de gpiozero.
Créez un objet 'ldr' pour la broche GPIO à laquelle vous avez connecté la photorésistance.
Utilisez la méthode when_dark pour déclencher votre impression. La commande d'impression peut être soit à l'intérieur d'une autre fonction, soit appelée en utilisant une fonction lambda.

## Faire du bruit

Il y a deux façons de faire en sorte que votre programme fasse du bruit plutôt que d'afficher simplement 'INTRUS'. La première consiste à utiliser un buzzer qui s'allume lorsque le faisceau laser est brisé. La seconde consiste à utiliser le module `pygame` pour diffuser un son dans des haut-parleurs.

Jetez un coup d'œil aux sections ci-dessous qui détaillent comment utiliser des buzzers et jouer des sons avec `pygame`. Choisissez la méthode que vous préférez, puis essayez de faire du bruit lorsque le rayon laser est cassé. Si vous voulez jouer des sons à travers les haut-parleurs, ce site en propose un grand nombre. Celui-ci en particulier pourrait vous être utile.

### Utilisation d'un buzzer avec un Raspberry Pi

Un buzzer piézoélectrique fonctionne en provoquant des vibrations minuscules et rapides d'un disque en céramique lorsqu'une tension lui est appliquée. Ces vibrations produisent à leur tour un son d'une fréquence unique.

!!! warning Attention

    Les buzzers piézoélectriques sont des composants polarisés, il est donc important de les placer dans vos circuits dans le bon sens.

#### Mise en place d'un buzzer piézoélectrique

Le câblage d'un buzzer piézoélectrique est très simple. Sur une planche à pain, il suffit de connecter la branche positive du buzzer à n'importe quelle broche GPIO de votre Raspberry Pi et la branche négative à une broche de terre. La branche positive est normalement la plus longue des deux, et la plupart des buzzers sont étiquetés pour montrer quel côté est positif.

![buzzer](img/ldr-buzzer.webp)

#### Codage de votre buzzer

Un buzzer piézoélectrique est un dispositif de sortie très simple. Vous pouvez utiliser la classe `Buzzer` de la librairie `gpiozero` pour l'allumer et l'éteindre. Le code ci-dessous suppose que le buzzer a été câblé sur le GPIO 17.

``` python
from gpiozero import Buzzer
buzzer = Buzzer(17)

buzzer.on()
```

!!!note Méthodes de la classe Buzzer
    La classe Buzzer possède les méthodes suivantes :

    ```python
    ## Allumer le buzzer
    buzzer.on()

    ## Éteindre le buzzer
    buzzer.off()

    ## Bip du buzzer (allumé et éteint pendant 1 seconde)
    buzzer.beep()

    ## Bip du buzzer pendant 0.1 secondes 10 fois
    buzzer.beep(0.1, 0.1, 10)
    ```

### Lecture de fichiers sonores avec Python

Pour lire un fichier son avec Python, vous pouvez utiliser un module appelé `pygame`. Il est préinstallé sur un Raspberry Pi.

#### Importation et initialisation de pygame

Vous devez d'abord importer le module `pygame` et l'initialiser.

``` python
import pygame
pygame.init()
```

#### Jouer un son

Ensuite, vous pouvez créer un objet Sound et lui fournir le chemin vers votre fichier.

``` python
mon_son = pygame.mixer.Sound('path/to/my/soundfile.wav')
```

Ensuite, vous pouvez jouer le son.

``` python
mon_son.play()
```

J'ai besoin d'un indice

## Construction de votre fil-piège laser

Une fois le circuit testé, vous pouvez câbler les composants directement sur le Raspberry Pi comme indiqué ci-dessous.

Placez une branche de la photorésistance et la longue branche du condensateur dans un câble de liaison femelle-femelle. Fixez-les avec du ruban adhésif.

Placez les autres pattes dans les fils de liaison, puis rebranchez le tout dans le Raspberry Pi.

Vous pouvez placer le Raspberry Pi et les composants dans un boîtier pour les dissimuler si vous le souhaitez. Ici, nous avons utilisé une boîte en plastique dans laquelle nous avons fait un trou pour la paille :

Placez votre boîtier près d'une porte. Fixez ensuite le pointeur laser sur le mur de façon à ce que le faisceau soit focalisé sur la paille.

Maintenant, exécutez le code et testez votre tripwire laser.

!!! hint Exécution automatique
    Si vous souhaitez exécuter votre code dès que le Raspberry Pi démarre, consultez le tutoriel sur [comment automatiser les tâches avec Cron](/linux/cron.md).

## Et ensuite ?

- Vous pourriez essayer d'inclure d'autres événements qui se déclenchent lorsque le fil-piège est rompu. Que diriez-vous d'envoyer un tweet, ou de prendre une photo de l'intrus ?
