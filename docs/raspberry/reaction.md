# Jeu de réaction rapide Python

## Introduction

Dans cette ressource, vous allez créer un jeu de réaction rapide à l'aide de quelques composants électroniques et d'un script Python.

Ce projet vous donne l'occasion d'utiliser l'électronique pour créer un jeu de réaction rapide que vous programmerez à l'aide de Python. Si vous avez peu ou pas d'expérience dans la création de circuits, ne vous inquiétez pas : ce guide vous guidera et, à la fin, vous aurez un jeu amusant pour jouer avec vos amis.

## Ce dont vous aurez besoin

- Matériel
  - 1 Raspberry Pi
  - 1 breadboard
  - 1 LED
  - 1 Résistance de 330 ohms
  - 4 fils de liaison mâle-femelle
  - 2 fils de liaison mâle-mâle
  - 2 boutons poussoirs tactiles

## Montage du circuit

Voici le circuit que vous allez construire, composé de deux boutons poussoirs et d'une LED.

![circuit](https://projects-static.raspberrypi.org/projects/python-quick-reaction-game/40ed13da3cfd84f8bf959bafc06dfc32da8a7e30/en/images/quick-reaction-circuit.png)

## Contrôle de la LED

Lorsque vous programmez, il est judicieux de s'attaquer à un seul problème à la fois. Il est ainsi plus facile de tester votre projet étape par étape.

- Ouvrez IDLE <!-- todo  finir-->

- Enregistrez le fichier sous le nom de `reaction.py` en cliquant sur <kbd>Fichier</kbd> puis <kbd>Enregistrer sous</kbd>.

Tout d'abord, vous devrez importer les modules et les bibliothèques nécessaires pour contrôler les broches GPIO du Raspberry Pi.

``` python
from gpiozero import LED, Button
from time import sleep
```

Vous devez configurer la broche du Raspberry Pi à laquelle la LED est connectée comme une sortie. Pour cela, créez une variable `led` pour nommer la broche, puis définissez la sortie :

``` python hl_lines="4"
from gpiozero import LED, Button
from time import sleep

led = LED(4)
```

Ensuite, ajoutez quelques lignes pour allumer la LED, attendre 5 secondes et éteindre la LED :

``` python hl_lines="6 7 8"
from gpiozero import LED, Button
from time import sleep

led = LED(4)

led.on()
sleep(5)
led.off()
```

Enfin, testez que cela fonctionne en cliquant sur <kbd>F5</kbd>.

Si la DEL ne s'allume pas pendant cinq secondes, revenez en arrière et essayez de trouver ce qui n'a pas fonctionné. Il s'agit d'une compétence très importante en informatique appelée débogage, qui consiste à trouver et à corriger les erreurs ou les bogues dans votre code.

## Ajouter un élément de surprise

Le but du jeu est de voir qui peut appuyer le premier sur le bouton lorsque la lumière s'éteint, il serait donc préférable que la durée d'allumage soit aléatoire. Vous devez ajouter et modifier quelques lignes de code dans votre programme Python pour y parvenir.

- En dessous de `from time import sleep` ajoutez une ligne pour importer la fonction `uniform` :

    ``` python hl_lines="3"
    from gpiozero import LED, Button
    from time import sleep
    from random import uniform

    led = LED(4)

    led.on()
    sleep(5)
    led.off()
    ```

    Ici, `uniform` permet la sélection aléatoire d'un nombre décimal (à virgule flottante) dans une plage de nombres.

- Localisez ensuite la ligne `sleep(5)` et modifiez-la pour qu'elle se lise comme suit :

    ``` python hl_lines="8"
    from gpiozero import LED, Button
    from time import sleep
    from random import uniform

    led = LED(4)

    led.on()
    sleep(uniform(5, 10))
    led.off()
    ```

- Sauvegardez votre travail en cliquant sur Save. Testez que tout fonctionne en cliquant sur Run

## Détection des boutons

La LED fonctionne ; vous voulez maintenant ajouter une fonctionnalité à votre programme afin de détecter l'appui sur un bouton. De cette façon, vous pouvez enregistrer les scores des joueurs pour voir qui gagne.

Comme précédemment un peu de code doit être ajouté à votre programme.

- Avec le fichier `reaction.py` ouvert, ajoutez les variables suivantes sous `led = LED(4)` :

    ``` python  hl_lines="6 7"
    from gpiozero import LED, Button
    from time import sleep
    from random import uniform

    led = LED(4)
    right_button = Button(15)
    left_button = Button(14)

    led.on()
    sleep(uniform(5, 10))
    led.off()
    ```

- Ensuite, sous `led.off()`, vous pouvez ajouter une fonction qui sera appelée à chaque fois qu'un bouton est pressé, et qui vous dira sur quelle broche se trouvait le bouton :

    ``` python hl_lines="13 14"
    from gpiozero import LED, Button
    from time import sleep
    from random import uniform

    led = LED(4)
    right_button = Button(15)
    left_button = Button(14)

    led.on()
    sleep(uniform(5, 10))
    led.off()

    def pressed(button):
        print(str(button.pin.number) + ' won the game')
    ```

- Pour finir, lorsque l'un des boutons est pressé, la fonction sera appelée. Si le bouton droit est enfoncé, vous pouvez envoyer la chaîne `'right'` à la fonction `pressed`. Si le bouton gauche est enfoncé, vous pouvez envoyer la chaîne `'left'`.

    ``` python hl_lines="16 17"
    from gpiozero import LED, Button
    from time import sleep
    from random import uniform

    led = LED(4)
    right_button = Button(15)
    left_button = Button(14)

    led.on()
    sleep(uniform(5, 10))
    led.off()

    def pressed(button):
        print(str(button.pin.number) + ' won the game')

    right_button.when_pressed = pressed
    left_button.when_pressed = pressed
    ```

- Sauvegardez votre programme et testez-le avec un ami.

## Obtenir les noms des joueurs

Ne serait-il pas préférable que le programme vous dise qui a gagné au lieu d'indiquer simplement le bouton sur lequel vous avez appuyé ? Pour cela, vous devez trouver le nom des joueurs. En Python, vous pouvez utiliser `input` pour cela.

- Pour connaître le nom des joueurs, vous pouvez utiliser `input` pour demander aux joueurs de taper leur nom.

    ``` python
    from gpiozero import LED, Button
    from time import sleep
    from random import uniform

    led = LED(4)
    right_button = Button(15)
    left_button = Button(14)

    left_name = input('left player name is ')
    right_name = input('right player name is ')

    led.on()
    sleep(uniform(5, 10))
    led.off()


    def pressed(button):
    print(str(button.pin.number) + ' won the game')


    right_button.when_pressed = pressed
    left_button.when_pressed = pressed
    ```

- Maintenant vous pouvez modifier la fonction `pressed` afin qu'elle affiche le nom du joueur qui a gagné.

    ``` python
    from gpiozero import LED, Button
    from time import sleep
    from random import uniform

    led = LED(4)
    right_button = Button(15)
    left_button = Button(14)

    left_name = input('left player name is ')
    right_name = input('right player name is ')

    led.on()
    sleep(uniform(5, 10))
    led.off()

    def pressed(button):
    if button.pin.number == 14:
    print(left_name + ' won the game')
    else:
    print(right_name + ' won the game')

    right_button.when_pressed = pressed
    left_button.when_pressed = pressed
    ```

- Exécutez votre programme et testez votre jeu pour voir s'il fonctionne.

- Vous remarquerez peut-être que le jeu ne se termine pas lorsque le bouton a été enfoncé. Cela peut être corrigé en ajoutant une sortie dans la fonction pressed. Ajoutez les lignes surlignées à votre code,

    ``` python
    from gpiozero import LED, Button
    from time import sleep
    from random import uniform
    from sys import exit

    led = LED(4)
    right_button = Button(15)
    left_button = Button(14)

    left_name = input("left player name is ")
    right_name = input("right player name is ")

    led.on()
    sleep(uniform(5, 10))
    led.off()

    def pressed(button):
        if button.pin.number == 14:
            print(left_name + " won the game")
        else:
            print(right_name + " won the game")
        exit()

    right_button.when_pressed = pressed
    left_button.when_pressed = pressed
    ```

## Et ensuite  ?

- Pouvez-vous placer le jeu dans une boucle (vous devrez supprimer la fonction exit()), de sorte que le voyant s'allume à nouveau ?
- Pouvez-vous ajouter des scores pour les deux joueurs qui s'accumulent sur un certain nombre de tours, et afficher les scores totaux des joueurs ?
- Pourquoi ne pas ajouter un chronomètre pour savoir combien de temps il a fallu aux joueurs pour appuyer sur le bouton après l'extinction de la LED ?

## Crédits et licence

Ce document est une adaptation du tutoriel officiel [Python Quick Reaction Game](https://projects.raspberrypi.org/en/projects/python-quick-reaction-game/) de la [fondation Raspberry Pi](https://www.raspberrypi.org/) disponible sous licence Creative Commons [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.fr).
