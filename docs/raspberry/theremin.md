# Theremin

[https://projects.raspberrypi.org/en/projects/ultrasonic-theremin/3](https://projects.raspberrypi.org/en/projects/ultrasonic-theremin/3)

## Ce que vous allez faire

Dans cette ressource, vous allez utiliser un capteur de distance à ultrasons pour contrôler les notes jouées par le Sonic Pi et libérer le Beach Boy qui sommeille en vous.

Ce que vous allez apprendre
En construisant un thérémine à ultrasons, vous apprendrez :

Comment détecter les distances avec un capteur de distance à ultrasons.
Comment communiquer des variables entre Sonic Pi et Python.

## Ce dont vous aurez besoin

!!! abstract Matériel
    - résistance de 330Ω
    - résistance 470Ω
    - 1 _breadboard_
    - capteur de distance à ultrasons
    - 3 cordons de liaison mâle-mâle
    - 4 fils de connexion mâle-femelle

Logiciel
Installation du logiciel
Ce projet repose sur la dernière version de Sonic Pi et de python-osc. Pour installer les logiciels dont vous avez besoin, exécutez les commandes suivantes dans une fenêtre de terminal :

``` console
sudo apt update && sudo apt upgrade -y
sudo pip3 install python-osc
```

## Le thérémine à ultrasons Raspberry Pi

Dans cette ressource, vous allez fabriquer votre propre thérémine en utilisant un capteur de distance à ultrasons et un peu de code Python et Sonic Pi.

Un thérémine est un instrument de musique unique, dans la mesure où il produit des sons sans être touché par l'interprète. Le circuit d'un thérémine est assez compliqué, mais vous pouvez le simuler en utilisant des capteurs de distance à ultrasons.

thérémine

La théréministe Lydia Kavina jouant à Ekaterinburg (G2pavlov à la Wikipedia de langue anglaise [GFDL ou CC-BY-SA-3.0], via Wikimedia Commons).

## Mise en place du circuit

Un capteur de distance à ultrasons est un dispositif qui envoie des impulsions de sons ultrasoniques et mesure le temps qu'elles mettent à rebondir sur des objets proches et à être réfléchies. Ils peuvent mesurer des distances assez précises, jusqu'à environ un mètre.

ultrasons

Un capteur de distance à ultrasons possède quatre broches. Elles sont appelées Masse (Gnd), Déclenchement (Trig), Echo (Echo) et Alimentation (Vcc).

Pour utiliser un capteur de distance à ultrasons, vous devez connecter la broche Gnd à la broche de terre du Raspberry Pi, la broche Trig à une broche GPIO du Raspberry Pi et la broche Vcc à la broche 5V du Raspberry Pi.

La broche Echo est un peu plus compliquée. Elle doit être connectée par une résistance de 330 ohms à une broche GPIO sur le Raspberry Pi, et cette broche doit être mise à la terre par une résistance de 470 ohms.

Le diagramme ci-dessous montre une suggestion d'arrangement pour configurer ceci.

circuit

## Détection de la distance

Grâce aux abstractions du module GPIO Zero, vous pouvez très facilement détecter la distance entre un objet et le capteur de distance. Si vous avez câblé le capteur comme indiqué sur le schéma, alors votre broche d'écho est 17 et votre broche de déclenchement est 4.

Cliquez sur Menu > Programmation > Python 3 (IDLE), pour ouvrir un nouveau shell Python.
Dans le shell, cliquez sur Nouveau > Nouveau fichier pour créer un nouveau fichier Python.
Le code pour détecter la distance est ci-dessous. Tapez-le dans votre nouveau fichier, puis enregistrez-le et exécutez-le.

```python
from gpiozero import DistanceSensor
from time import sleep

sensor = DistanceSensor(echo=17, trigger=4)

while True:
    print(sensor.distance)
    sleep(1)
```

La distance sensor.distance est la distance en mètres entre l'objet et le capteur. Exécutez votre code et déplacez votre main d'avant en arrière devant le capteur. Vous devriez voir la distance changer, comme elle est imprimée dans le shell.

## Préparer Sonic Pi

Sonic Pi va recevoir des messages de votre script Python. Celui-ci indiquera à Sonic Pi quelle note jouer.

Ouvrez Sonic Pi en cliquant sur Menu > Programmation > Sonic Pi.

Dans le tampon qui est ouvert, vous pouvez commencer par écrire une boucle live_loop. Il s'agit d'une boucle qui s'exécute indéfiniment, mais qui peut facilement être mise à jour, ce qui vous permet d'expérimenter différents sons. Vous pouvez également ajouter une ligne pour réduire le temps nécessaire à Sonic Pi et à Python pour communiquer entre eux.

``` ruby
live_loop :listen do
    use_real_time
end
```

Ensuite, vous pouvez synchroniser la boucle en direct avec les messages qui proviendront de Python.

``` ruby
live_loop :listen do
    use_real_time
    note = synchronisation "/osc/play_this
end
```

Le message qui arrive sera une liste, la note étant le 0e élément.

``` ruby
live_loop :listen do
    use_real_time
    note = synchro "/osc/play_this
    joue la note[0]
end
```

Vous pouvez lancer la lecture de cette boucle en direct en cliquant sur le bouton Run. Vous n'entendrez rien pour l'instant, car la boucle ne reçoit aucun message.

## Envoi de notes à partir de Python

Pour terminer votre programme, vous devez envoyer les valeurs midi des notes au Sonic Pi à partir de votre fichier Python.

Vous devrez utiliser la bibliothèque OSC pour cette partie. Il y a deux importations à ajouter en haut de votre fichier, pour permettre à Python d'envoyer des messages.

```python
from gpiozero import DistanceSensor
from time import sleep

from pythonosc import osc_message_builder
from pythonosc import udp_client

sensor = DistanceSensor(echo=17, trigger=4)

while True :
    print(sensor.distance)
    sleep(1)
```

Maintenant vous devez créer un objet expéditeur qui peut envoyer le message.

``` python
from gpiozero import DistanceSensor
from time import sleep

from pythonosc import osc_message_builder
from pythonosc import udp_client

sensor = DistanceSensor(echo=17, trigger=4)
sender = udp_client.SimpleUDPClient('127.0.0.1', 4559)

while True :
    print(capteur.distance)
    sleep(1)
```

Vous devez convertir la distance en une valeur midi. Celles-ci doivent être des nombres entiers, et tourner autour de la valeur 60, qui est le do moyen. Pour ce faire, vous devez arrondir la distance à un nombre entier, la multiplier par 100 et ensuite ajouter un petit peu, afin que la note ne soit pas trop basse.

``` python
from gpiozero import DistanceSensor
from time import sleep

from pythonosc import osc_message_builder
from pythonosc import udp_client

sensor = DistanceSensor(echo=17, trigger=4)
sender = udp_client.SimpleUDPClient('127.0.0.1', 4559)

while True :
    pitch = round(sensor.distance * 100 + 30)
    sleep(1)
```

Pour finir, vous devez envoyer la distance à Sonic Pi et réduire le temps de sommeil.

``` python
from gpiozero import DistanceSensor
from time import sleep

from pythonosc import osc_message_builder
from pythonosc import udp_client

sensor = DistanceSensor(echo=17, trigger=4)
sender = udp_client.SimpleUDPClient('127.0.0.1', 4559)

while True :
    pitch = round(sensor.distance * 100 + 30)
    sender.send_message('/play_this', pitch)
    sleep(0.1)
```

Sauvegardez et exécutez votre code et voyez ce qui se passe. Si tout va bien, vous avez fabriqué votre propre thérémine.

## Et ensuite ?

- Un vrai thérémine possède un contrôle secondaire pour changer l'amplitude (volume) du son joué. Vous pourriez le faire avec un deuxième capteur de distance à ultrasons.

- Que se passe-t-il si vous modifiez le timing dans le fichier Python ? Pouvez-vous produire une transition plus douce des notes ?

- Jouez avec différents synthés, comme indiqué dans le menu d'aide de Sonic Pi. Quel effet le changement de synthétiseur a-t-il sur votre thérémine ?
