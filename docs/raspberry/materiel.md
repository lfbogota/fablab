# Matériel

| Fourniture                | Quantité  |
|--------------------------------|--:|
| Raspberry 4 2GB                |   |
| Raspberry zéro W                |   |
| Boitier blanc                  |   |
| Alimentation                   |   |
| Micro SD 16 Go                 |   |
| Camera RPI 5 MP                |   |
| Capteur pression atmosphérique `GY-68 BMP180` |   |
| Capteur distance ultra sons    `HY-SRF05` |   |
| Capteur température / humidité `DHT22` |   |
| LED Vert                       |   |
| LED Rouge                      |   |
| LED Jaune                      |   |
| LED RGB ||
| Protoboard                     |   |
| Servomoteur `SG90`                    |   |
| Condensateur 1 µF               |   |
| Résistance 1 kΩ               |   |
