# Commander un Raspberry Pi à distance

## Accès à distance

Parfois, vous avez besoin d'accéder à un Raspberry Pi sans le connecter à un écran. Peut-être que le Pi est intégré dans quelque chose comme un robot, ou que vous voulez voir certaines informations depuis un autre endroit. Ou peut-être n'avez-vous tout simplement pas d'écran disponible !

Vous pouvez vous connecter à votre Raspberry Pi depuis une autre machine. Mais pour ce faire, vous devez connaître l'adresse IP du Raspberry Pi.

Tout périphérique connecté à un réseau local se voit attribuer une adresse IP. Pour vous connecter à votre Raspberry Pi depuis une autre machine à l'aide de SSH, vous devez connaître l'adresse IP du Pi. Cela est facile si vous avez un écran connecté, et dans le cas contaire il existe un certain nombre de méthodes pour la trouver à distance depuis une autre machine sur le réseau.

## Comment trouver votre adresse IP

Il est possible de trouver l'adresse IP de votre Pi sans vous connecter à un écran en utilisant l'une des méthodes suivantes :

!!!tip "Remarque"

    Si vous utilisez un écran avec votre Raspberry Pi et si vous démarrez sur la ligne de commande au lieu du bureau, votre adresse IP devrait être affichée dans les derniers messages avant l'invite de connexion. Sinon, ouvrez une fenêtre Terminal et tapez `hostname -I`, ce qui révélera l'adresse IP de votre Raspberry Pi.
