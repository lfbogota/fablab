# Publier sur Mastodon avec un bot

## Le réseau Mastodon

## Inscription

## Configuration

Une fois le profil créé, nous voulons maintenant obtenir le code d'accès appelé _acces token_ qui nous permettra de poster de nouveaux status depuis un programme écrit en Python. La première chose à faire est d'ajouter une application à notre compte.

- Dans le menu `Preferences`, cliquez sur `Development page` puis sur `New Application` ([lien](https://botsin.space/settings/applications)). Choisissez un nom pour votre application.

    ![new bot](img/mastodon-new.png)

- Au niveau des permissions cochez uniquement `write:statuses` car notre bot va se limiter à poster de nouveaux status.

    ![permissions](img/mastodon-permissions.png)

- Voici ce que vous devriez obtenir si votre application est créée avec succès :

    ![success](img/mastodon-app.png)

- Le code d'accès ou _access token_ est maintenant disponible dans la page de configuration :

    ![acces token](img/mastodon-secret.png)

    Ce code d'accès est ce qui va nous permettre à notre bot d'interagir avec notre compte Mastodon.

    !!! danger "Danger"

        Veuillez tenir compte de l'avertissement vous demandant d'_être très prudent avec ces données et de ne jamais les partager_. Si les informations d'identification sont compromises, n'importe qui sera en mesure de prendre le contrôle de ce compte. C'est pourquoi il est important que cette application ne dispose que des autorisations minimales dont elle a besoin pour fonctionner.

## Poster sur Mastodon avec Python

### Installation de la librairie

Les interactions avec Mastodon vont se faire à l'aide de la librairie python `Mastodon.py`. Cette librairie n'est pas dans la distribution standard de Python et doit être installée à partir d'une fenêtre de Terminal. Ouvrez une fenêtre de Terminal et entrez la ligne suivante :

```console
pip3 install Mastodon.py
```

### Hello world

Pour tester le fonctionnement de la librairie, nous allons poster un premier statut avec le message _Hello world!_. Exécuter le code ci-dessous après avoir remplacé `'????????????????????'` par votre _acces token_.

```python
from mastodon import Mastodon

mastodon = Mastodon(
    access_token = '????????????????????????????????????????',
    api_base_url = 'https://botsin.space/'
)

mastodon.status_post("Hello world!")
```

!!!warning "Limites"

    Les serveur Mastodon limitent le nombre de statut qui peut être posté chaque minutes afin d'éviter les abus comme le _spam_. Par défaut, la limite est fixée à 300 requêtes par tranche de 5 minutes (c'est à dire 1 requête par seconde).

    Dans le cas où vous utilisez une boucle `for` ou `while`, il faudra faire attention à utiliser une instruction `sleep(1)` pour demander au programme de faire une pause pendant 1 seconde avant de lancer la requête suivante.

### Poster une image

Pour ajouter une image à notre statut, il faut ajouter un paramètre `media_ids` lors de l'appel de la méthode `status_post`.

``` python
media = mastodon.media_post("test.png")
mastodon.status_post("Notre première photo !", media_ids=media)
```

### Effacer un statut

#### par l'id

``` python hl_lines="1"
{'id': 106967222167645173,
 'created_at': datetime.datetime(2021, 9, 21, 2, 11, 19, 661000, tzinfo=tzutc()),
 'in_reply_to_id': None,
 'in_reply_to_account_id': None,
 'sensitive': False,
 ...
}
```

``` python
mastodon.status_delete(106967222167645173)
```

#### Remise à zéro complète

!!! danger "Danger"
    Le code ci-dessous est une boucle `for` qui va effacer tous vos statuts un par un. Exécuter ce programme seulement si vous souhaitez remettre votre compte totalement à zéro.

``` python
from time import sleep

me = mastodon.me()
mastodon.account_statuses(me)

for status in mastodon.account_statuses(me):
    mastodon.status_delete(status)
    sleep(1)
```

#### Remise à zéro partielle

``` python hl_lines="8"
from time import sleep

me = mastodon.me()
mastodon.account_statuses(me)

for status in mastodon.account_statuses(me):
    if status['application']['name'] == 'monbot_test':
        mastodon.status_delete(status)
        sleep(1)
```
