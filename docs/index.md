# Fablab objets connectés

![aa](iot.png)

<!--
```mermaid
flowchart LR
    RP{Raspberry Pi}
        B(bouton) --> RP
    T(thermomètre) --> RP
    CA(caméra) --> RP
    US(capteur de distance) --> RP
    A(capteur de lumière) --> RP
    V(microphone) --> RP
    X(...) --> RP
    RP --> D[écran]
    RP --> E[site web]
    RP --> F[LED]
    RP --> G[servo-moteur]
    RP --> H[réseau social]
    RP --> HP[haut parleur]
    RP --> Z(...)

```
-->